package tech.arno.gittest;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

/**
 * <pre>
 *     author: ArnoFrost
 *     time  : 2017/3/24
 *     desc  : PhotoActivity
 * </pre>
 */
public class PhotoActivity extends AppCompatActivity {

    public static final String PHOTO_NAME = "photo_name";
    public static final String PHOTO_IMAGE_ID = "photo_image_id";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        Intent inetent = getIntent();
        String photoName = inetent.getStringExtra(PHOTO_NAME);
        int photoImageId = inetent.getIntExtra(PHOTO_IMAGE_ID, 0);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout)
                findViewById(R.id.collapsing_toolbar);
        ImageView photoImageView = (ImageView) findViewById(R.id.image_view);
//        TextView photoText = (TextView) findViewById(R.id.photo_content_text);


        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        collapsingToolbarLayout.setTitle(photoName);

        Glide.with(this).load(photoImageId).into(photoImageView);
//        String photoContent = generatePhotoContent(photoName);
//        String photoContent = "每个美好的瞬间都有你的陪伴。";
//        photoText.setText(photoContent);
    }

//    private String generatePhotoContent(String photoName) {
//        StringBuilder photoContent = new StringBuilder();
//        for (int i = 0; i < 500; i++) {
//            photoContent.append(photoName);
//
//        }
//        return photoContent.toString();
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
