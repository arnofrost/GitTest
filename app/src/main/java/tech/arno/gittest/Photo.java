package tech.arno.gittest;

/**
 * <pre>
 *     author: ArnoFrost
 *     time  : 2017/3/24
 *     desc  : Photo
 * </pre>
 */
public class Photo {
    private String name;
    private int imageId;

    public String getName() {
        return name;
    }


    public int getImageId() {
        return imageId;
    }


    public Photo(String name, int iamgeId) {
        this.name = name;
        this.imageId = iamgeId;
    }
}
