package tech.arno.gittest;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * <pre>
 *     author: ArnoFrost
 *     time  : 2017/3/24
 *     desc  : PhotoAdapter
 * </pre>
 */
public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.ViewHolder> {

    private Context mContext;

    private List<Photo> mPhotoList;


    static class ViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        ImageView image;
        TextView name;

        public ViewHolder(View view) {
            super(view);
            cardView = (CardView) view;
            image = (ImageView) view.findViewById(R.id.image);
            name = (TextView) view.findViewById(R.id.name);

        }
    }

    public PhotoAdapter(List<Photo> photoList) {
        mPhotoList = photoList;
    }

//    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//
//        if (mContext == null) {
//            mContext = parent.getContext();
//        }
//        View view = LayoutInflater.from(mContext).
//                inflate(R.layout.card_item, parent, false);
//        final ViewHolder holder = new ViewHolder(view);
//        holder.cardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int position = holder.getAdapterPosition();
//                Photo photo = mPhotoList.get(position);
//                Intent intent = new Intent(mContext, PhotoActivity.class);
//                intent.putExtra(PhotoActivity.PHOTO_NAME, photo.getName());
//                intent.putExtra(PhotoActivity.PHOTO_IMAGE_ID, photo.getImageId());
//                mContext.startActivity(intent);
//            }
//        });
//        return new ViewHolder(view);
//    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null) {
            mContext = parent.getContext();
        }
        View view = LayoutInflater.from(mContext).inflate(R.layout.card_item, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                Photo fruit = mPhotoList.get(position);
                Intent intent = new Intent(mContext, PhotoActivity.class);
                intent.putExtra(PhotoActivity.PHOTO_NAME, fruit.getName());
                intent.putExtra(PhotoActivity.PHOTO_IMAGE_ID, fruit.getImageId());
                mContext.startActivity(intent);
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Photo photo = mPhotoList.get(position);
        holder.name.setText(photo.getName());
        Glide.with(mContext).load(photo.getImageId()).into(holder.image);

    }

    @Override
    public int getItemCount() {
        return mPhotoList.size();
    }

}
